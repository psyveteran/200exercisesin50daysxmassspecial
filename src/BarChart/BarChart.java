package BarChart;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BarChart {

    class Store {

        private final Integer STORE_ID_NUMBER;
        private Double salesGrossTotal;

        public Store(final Integer ID, Double sales) {
            STORE_ID_NUMBER = ID;
            salesGrossTotal = sales;
        }

        public Integer getSTORE_ID_NUMBER() {
            return STORE_ID_NUMBER;
        }

        public Integer getStoreSalesLoad() {
            return ((Double)(getSalesGrossTotal() / 100)).intValue();
        }

        public Double getSalesGrossTotal() {
            return salesGrossTotal;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Store [");
            sb.append(" STORE_ID_NUMBER = ").append(getSTORE_ID_NUMBER());
            sb.append(", sales Gross Totals = $").append(String.format("%.2f", getSalesGrossTotal()));
            sb.append(" ]\n\r");
            return sb.toString();
        }
    }

    private Store[] storeManifest;
    private Scanner scanner;


    public BarChart(int numberOfStores) {
        storeManifest = new Store[numberOfStores];
        scanner = new Scanner(System.in);

        queryStoreSales(numberOfStores);
        reportBarChart();
    }

    private void reportBarChart() {
        System.out.println("\nReport on sales targets:");

        for(Store store : storeManifest) {
            System.out.print("Store #" + store.getSTORE_ID_NUMBER() + ": ");

            for (int astrix = store.getStoreSalesLoad(); astrix > 0; --astrix) {
                System.out.print('*');
            }
            System.out.print('\n');
        }
    }

    private void queryStoreSales(int numberOfStores) {
        numberOfStores = Math.abs(numberOfStores);

        while (numberOfStores > 0) {
            System.out.println("Please enter the Gross sales for store #" + (numberOfStores));
            System.out.print("$  ");

            Double input = 0d;

            try {
                input = scanner.nextDouble();
            } catch (InputMismatchException ime) {
                System.err.println("input error, resetting back to re-evaluate store #");
                // Cleanup
                scanner.nextLine();  // purge fouling line.
                numberOfStores = (numberOfStores > 0)? ++numberOfStores : 0; // re-loop for another try.
            }

            storeManifest[numberOfStores - 1] = new Store(numberOfStores, input);    // effectively fills backwards
            --numberOfStores;
        }
    }

}
