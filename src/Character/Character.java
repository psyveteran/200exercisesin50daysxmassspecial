package Character;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Character {

    private String dataGiven;
    private char searchIota;
    private Scanner scanner;

    public Character() {
        this.dataGiven = " ";
        scanner = new Scanner(System.in);

        prompt();
        characterAssessment();
    }

    private void characterAssessment() {
        long count = 0;
        for(char c : dataGiven.toCharArray()) {
            if (c == searchIota) count++;
        }

        System.out.printf("There %s %d occurrence%s of '%c' in the given sentence.",
                (count > 1)? "are": "is",
                count,
                (count > 1)? "s" : "",
                searchIota);
    }

    private void prompt() {
        System.out.println("Enter in a sentence followed by the enter key.");

        try {
            dataGiven = scanner.nextLine().trim();
        } catch (NoSuchElementException nse) {
            nse.printStackTrace();
        }

        System.out.println("Enter a character to index, followed by the enter key.");

        try {
            searchIota = scanner.next().trim().charAt(0);
            scanner.nextLine();
        } catch (NoSuchElementException nse) {
            nse.printStackTrace();
        }
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Character{");
        sb.append("dataGiven='").append(dataGiven).append('\'');
        sb.append(", searchIota=").append(searchIota);
        sb.append('}');
        return sb.toString();
    }
}
