package Budget;


import java.math.BigDecimal;

public class Budget {

    private enum state {
        GREEN, BLACK, RED, GREY
    }

    private BigDecimal monthlyBudget;
    private BigDecimal monthlyExpediture;

    protected Budget(BigDecimal monthlyBudget) {
        this.monthlyBudget = monthlyBudget;
        monthlyExpediture = BigDecimal.valueOf(0d);
    }

    public boolean addExpenditure(final BigDecimal expenditure) {
        monthlyExpediture = monthlyExpediture.add(expenditure);

        return true;
    }

    public BigDecimal getBudgetValue() {
        return monthlyBudget.subtract(monthlyExpediture);
    }

    public state getState() {
        state currentState = state.GREY;
        switch (getBudgetValue().signum()) {
            case -1:
                currentState = state.RED;
                break;
            case 0:
                currentState = state.BLACK;
                break;
            case 1:
                currentState = state.GREEN;
                break;
        }
        return currentState;
    }


}
