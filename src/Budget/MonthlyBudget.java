package Budget;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MonthlyBudget {

    private Budget budget;
    private Scanner scanner;

    public MonthlyBudget() {
        scanner = new Scanner(System.in);

        promptBudget();
        promptExpenses();
    }

    private void promptBudget() {
        BigDecimal value = getBigDecimalScanner("Please enter your budget for the month: ");
        budget = new Budget(value);
    }

    private BigDecimal getBigDecimalScanner(String message) {
        BigDecimal value = BigDecimal.ZERO;
        do {
            System.out.println(message);
            try {
                value = scanner.nextBigDecimal().abs();
                scanner.nextLine();
            } catch (InputMismatchException ime) {
                System.out.println("Input does not match pattern: '0.00' Try again.");
                scanner.nextLine();
            }
        } while(value.equals(BigDecimal.ZERO));
        return value;
    }

    private void promptExpenses() {
        boolean expenseLoop = true;
        do {
            System.out.print("Enter an expenses of this month or type 'done'");
            if (scanner.hasNext("done")) {
                expenseLoop = false;
            } else {
                budget.addExpenditure(getBigDecimalScanner(""));
            }
        } while (expenseLoop);

        // Budget and expenses have been gathered, report back.
        budgetReport();
    }

    private void budgetReport() {
        System.out.printf("The Budget is in the %s by: $%.2f", budget.getState(), budget.getBudgetValue());
    }
}
