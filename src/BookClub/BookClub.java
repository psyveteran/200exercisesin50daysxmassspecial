package BookClub;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

public class BookClub {

    private Vector<Customer> salesJournal;
    private Scanner scanner;

    public BookClub() {
        scanner = new Scanner(System.in);
        salesJournal = new Vector<Customer>();

        prompt();
    }

    private void prompt() {
        System.out.println("How many books has this customer purchased?");

        int bookCount = 0;
        do {
            try {
                bookCount = scanner.nextInt(); scanner.nextLine();
            } catch (InputMismatchException ime) {
                System.out.println("Invalid book quantity given, please try again.");
                scanner.nextLine();
            }
        } while (bookCount == 0);


        // Create a customer based on just books purchased for now.
        salesJournal.add(new Customer(bookCount));

        // Any further customers?
        System.out.println("Is there another customer (Y/N) ?");
        promptPOS(scanner.next().charAt(0));
        scanner.nextLine();
    }

    private void promptPOS(char state) {
        switch (state) {
            case 'Y':
            case 'y':
                prompt();
                break;
            case 'N':
            case 'n':
                System.out.println(Arrays.deepToString(salesJournal.toArray()));
                break;
            default:
                scanner.nextLine();
                System.out.printf("Incorrect input, please try again (Y/N)?");
                promptPOS(scanner.next().charAt(0));
                break;
        }
    }
}
