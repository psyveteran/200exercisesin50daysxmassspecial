package BookClub;

public class Customer {

    private Integer booksPurchased;
    private Integer pointsAcquired;

    Customer(Integer booksPurchased) {
        this.booksPurchased = booksPurchased;
        this.pointsAcquired = calculatePoints(booksPurchased);
    }

    Customer() {
        booksPurchased  = 0;
        pointsAcquired = 0;
    }

    /**
     * Attributes points based on books purchased.
     * @param booksPurchased
     * @return points attributed to booksPurchased or 0 if returns/trades
     */
    private Integer calculatePoints(final Integer booksPurchased) {
        Integer points = 0;

        if (Math.signum(booksPurchased) > 0) {
            switch (booksPurchased) {
                case 1:
                    points = 5;
                    break;
                case 2:
                    points = 15;
                    break;
                case 3:
                    points = 30;
                    break;
                default:    // 4 books and above.
                    points = 60;
                    break;
            }
        }
        return points;
    }

    public void setBooksPurchased(Integer booksPurchased) {
        this.booksPurchased = booksPurchased;
        pointsAcquired += calculatePoints(booksPurchased);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Customer { ");
        sb.append("books purchased: ").append(booksPurchased);
        sb.append(", points acquired: ").append(pointsAcquired);
        sb.append("} \n");

        return sb.toString();
    }
}


