package AlphabeticalPhoneTranslator;


import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;

public class AlphabeticalPhoneTranslator {


    enum values {
        ABC, DEF, GHI, JKL, MNO, PQRS, TUV, WXYZ
    }

    private Vector<String> workingValues;    // Vector to contain user given input.
    private Scanner readIn;

    public AlphabeticalPhoneTranslator() {
        workingValues = new Vector<>(15);    // 15 ~ slightly above average phone number length.
        readIn = new Scanner(System.in);

        prompt();
        translateAlphaToNumeric();
    }

    /**
     * Prompts the user for input and populates workingValues
     */
    public void prompt() {
        System.out.println("Please enter a phone number for translation, followed by the enter key:  ");

        workingValues.addAll(Arrays.asList(readIn.nextLine().split("")));
        readIn.close();

        System.out.printf("You have entered: %s\n", workingValues.toString());
    }

    /**
     * For all String value (names) in enum, looks to match substring parameter.
     * @param substring 1 length string for comparing.
     * @return integer value of position substring appears in enum shifted +2
     */
    private int mapToEnum(String substring) {
        for (int x = 0; x < values.values().length ; x++) {
            if (values.values()[x].name().contains(substring.toUpperCase())) {
                return x + 2;
            }
        }
        return 0;
    }

    /**
     * drives mapToEnum(String) and mutates vector positions as required.
     */
    private void translateAlphaToNumeric() {
        for (int x = 0; x < workingValues.size(); x++) {
            int outcome = mapToEnum(workingValues.elementAt(x));

            if (outcome > 0) {
                workingValues.set(x, outcome + "");
            }
        }

        System.out.printf("\nThe translated phone number is: %s", workingValues.toString());
    }
}
