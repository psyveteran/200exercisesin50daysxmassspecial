package CarInstrument;


public class Odometer {

    private long mileage;
    private final long MAX_ODOMETER = 999999L;
    private final int FUEL_CONSUMPTION_RATE = 24;

    private FuelGauge fuelGauge;

    public Odometer(FuelGauge fuelGauge) {
        this.fuelGauge = fuelGauge;
    }

    public long getMileage() {
        return mileage;
    }

    public void incrementMiles() {
        mileage = (++mileage > MAX_ODOMETER)? 0 : mileage;

        // Decrement fuel reserves by order of fuel consumption rate.
        if ((mileage % 24) == 0 ) {
            if (fuelGauge.decrementFuel()) {
                System.out.println("  !!  EMPTY TANK"); // TODO handle having an empty tank.
            }
        }
    }




}
