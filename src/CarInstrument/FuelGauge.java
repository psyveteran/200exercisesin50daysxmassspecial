package CarInstrument;

public class FuelGauge {

    private final double FUEL_CAPACITY = 15d;

    private double gallonsOfFuel;

    public FuelGauge() {
        gallonsOfFuel = FUEL_CAPACITY;
    }

    public double getFuel() {
        return gallonsOfFuel;
    }

    public boolean incrementFuel() {
        boolean continueFilling = true;

        if (++gallonsOfFuel >= FUEL_CAPACITY) {
            gallonsOfFuel = FUEL_CAPACITY;
            continueFilling = false;
        }
        return continueFilling;
    }

    public boolean decrementFuel() {
        boolean isEmpty = false;

        if (--gallonsOfFuel <= 0d) {
            gallonsOfFuel = 0d;
            isEmpty = true;
        }
        return isEmpty;
    }

}
