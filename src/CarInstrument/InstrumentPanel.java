package CarInstrument;

public class InstrumentPanel {

    private Odometer odometer;
    private FuelGauge fuelGauge;

    public InstrumentPanel() {
        fuelGauge = new FuelGauge();
        odometer = new Odometer(fuelGauge);


        testDrive(); // drive 1
        partFill(Math.random() * 11); // filling station
        testDrive();  //drive 2
    }

    private void partFill(double fuel) {
        System.out.println("## filling back up");
        while (--fuel > 0) fuelGauge.incrementFuel();
    }

    private void testDrive() {
        while (fuelGauge.getFuel() > 0) {
            System.out.printf("Miles driven: %d fuel remaining: %.2f \n", odometer.getMileage(), fuelGauge.getFuel());
            odometer.incrementMiles();
        }
    }


}
