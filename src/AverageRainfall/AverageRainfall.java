package AverageRainfall;

import java.time.Month;
import java.time.Year;
import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class AverageRainfall {

    class Rainfall {

        private Year year;
        private EnumMap<Month, Double> mappedRainfall;

        public Rainfall() {
            mappedRainfall = new EnumMap<>(Month.class);
        }

        public Year getYear() {
            return year;
        }

        public Double getTotalRainfall() {
            return mappedRainfall.values().stream().reduce(0d, Double::sum);
        }

        public void setYear(Year year) {
            this.year = year;
        }

        public void mapRainfall(Month month, Double rainfall) {
            mappedRainfall.put(month, rainfall);
        }

        public double getAverageRainfall() {
            return (getTotalRainfall() > 0? (getTotalRainfall() / 12d) : 0d);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
                sb.append("For the year '").append(getYear()).append("' ");
                sb.append("average rainfall was:  ").append(String.format("%.2f", getAverageRainfall())).append(" mm\n");

                for(Map.Entry<Month, Double> set : mappedRainfall.entrySet()) {
                    sb.append("Rainfall in the month of: ")
                            .append(set.getKey())
                            .append(" totaled: ")
                            .append(String.format("%.2f", set.getValue()))
                            .append(" mm\n");
                }

                sb.append("\n");

            return sb.toString();
        }
    }

    private Rainfall[] rainfallLog;
    private Scanner scanner;

    public AverageRainfall() {

        System.out.printf("How many years to set rainfall volumes? ");
        scanner = new Scanner(System.in);
        rainfallLog = new Rainfall[(scanner.nextInt())]; scanner.nextLine();

        for (int x = 0; x < rainfallLog.length ; x++) {
            rainfallLog[x] = collectYearlyData();
        }

        scanner.close();
        relayInput();
    }

    private Rainfall collectYearlyData() {
        Rainfall rainfall = new Rainfall();

        // Prompt user for year.
        System.out.println("Please enter the year (eg: 1995) for records: ");
        rainfall.setYear(Year.of(scanner.nextInt()));

        // Prompt user for each month of defined year.
        for (int currentMonth = 1; currentMonth < 13 ; currentMonth++) {
            System.out.printf("For The month of %s, what was the total rainfall in mm? ", Month.of(currentMonth));

            rainfall.mapRainfall(Month.of(currentMonth), Math.abs(scanner.nextDouble())); scanner.nextLine();
        }

        return rainfall;
    }

    private void relayInput() {
        System.out.println("\n-- Rainfall Report --\n");

        for(Rainfall rainfall : rainfallLog) {
            System.out.println(rainfall.toString());
        }
    }
}
