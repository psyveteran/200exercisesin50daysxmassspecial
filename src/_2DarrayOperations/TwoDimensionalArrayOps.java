package _2DarrayOperations;


import java.util.Arrays;

//// TODO: 17/11/2016  below.
/*
• Write a Junit unit test to validate the logic if each method behaves as you expect
• Instead of passing a two-dimensional array and a subscript, refactor to use the appropriate min, max, sum and count methods.
• Fix all output to be two decimal places by writing a method that accepts a double and returns a formatted string.
 */

public class TwoDimensionalArrayOps {

    private int[][] array;

    public TwoDimensionalArrayOps() {
        this.array = new int[][]{{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16},{17,18,19,20},{21,22,23,24}};

        output();
    }

    private void output() {
        System.out.println("Surface level test data is : " + this.toString());
        System.out.println("Get totals ~> " + getTotal(array));
        System.out.println("Get average ~> " + getAverage(array));

        System.out.println("----------------------------------");

        for (int i = 0; i < array.length; i++) {
            System.out.println("Total of row: " + i + " ~> " + getRowTotal(array, i));
        }

        System.out.println("----------------------------------");

        for (int i = 0; i < array.length; i++) {
            System.out.println("Total of col: " + i + " ~> " + getColumnTotal(array, i));
        }

        System.out.println("----------------------------------");

        for (int i = 0; i < array.length; i++) {
            System.out.println("Highest in row: " + i + " ~> " + getHighestInRow(array, i));
        }

        System.out.println("----------------------------------");

        for (int i = 0; i < array.length; i++) {
            System.out.println("Lowest in row: " + i + " ~> " + getLowestInRow(array, i));
        }
    }

    public int getTotal(final int[][] array) {
        int total = 0;

        for (int rowIndex = 0; rowIndex < array.length; rowIndex++) {
            total += getRowTotal(array, rowIndex);
        }

        return total;
    }

    public double getAverage(final int[][] array) {
        double sample = 0;

        for(int[] subArray : array) {
             sample += Arrays.stream(subArray).count();
        }

        return (getTotal(array) / sample);
    }

    public int getRowTotal(final int[][] array, int rowIndex) {
        if (rowIndex >= 0 && rowIndex < array.length) {

            return Arrays.stream(array[rowIndex]).sum();
        }
        return -1;
    }

    public int getColumnTotal(final int[][] array, int columnIndex) {
        int[] colCounter = new int[array.length];

        for (int rowIndex = 0; rowIndex < colCounter.length; rowIndex++) {

            // Add int value to colCounter[] if the columnIndex is a valid index for the current subArray.
            if (columnIndex < array[rowIndex].length && columnIndex >= 0) {
               colCounter[rowIndex] = array[rowIndex][columnIndex];
           }
        }

        return Arrays.stream(colCounter).sum();
    }

    public int getHighestInRow(final int[][] array, int rowIndex) {
        if (rowIndex < array.length && rowIndex >= 0) {
            int maxValue = 0;

            for (int value : array[rowIndex]) {
                if (value > maxValue) {
                    maxValue = value;
                }
            }

            return maxValue;
        }
        return -1;
    }

    public int getLowestInRow(final int[][] array, int rowIndex) {
        if (rowIndex < array.length && rowIndex >= 0) {
            int minValue = array[rowIndex][0];

            for (int value : array[rowIndex]) {
                if (value < minValue) {
                    minValue = value;
                }
            }

            return minValue;
        }
        return -1;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(array);
    }
}
