package CelsiusToFahrenheit;

import org.junit.Test;

import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;


public class TemperatureTest {

    // exact temp crossover point values.
    protected int celsius = 95;
    protected int fahrenheit = 35;

    @Test
    public void basicTestFtoC() throws Exception {
        System.out.println("basic 0 -> 100 int test (F -> C)");
        for (int temp = 0; temp < 100; temp++) {
            System.out.printf("%d Fahrenheit ~>\t\t%.5f Celsius\n",
                    temp, Temperature.getCelsius(temp, RoundingMode.HALF_EVEN));
        }
    }

    @Test
    public void basicTestCtoF() throws Exception {
        System.out.println("basic 100 -> 0 int test (C -> F)");
        for (int temp = 100; temp >= 0; temp--) {
            System.out.printf("%d Celsius ~>\t\t%.5f Fahrenheit\n",
                    temp, Temperature.getCelsius(temp, RoundingMode.HALF_EVEN));
        }
    }

    @Test
    public void getCelsius() throws Exception {
        assertEquals(celsius, Temperature.getCelsius(fahrenheit, RoundingMode.UNNECESSARY), 0d);
    }

    @Test
    public void getFahrenheit() throws Exception {
        assertEquals(fahrenheit, Temperature.getFahrenheit(celsius, RoundingMode.UNNECESSARY), 0d);
    }

}