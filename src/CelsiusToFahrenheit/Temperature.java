package CelsiusToFahrenheit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Temperature {

    private enum Conversion {
        SPLIT(32d), NINE(9d), FIVE(5d);

        private BigDecimal value;

        Conversion(double value) {
            this.value = BigDecimal.valueOf(value);
        }

        protected BigDecimal getValue() {
            return value;
        }
    }

    public static double getCelsius(double fahrenheit, RoundingMode rounding) {
        BigDecimal value = BigDecimal.valueOf(fahrenheit);

        value = value.multiply(Conversion.NINE.getValue());
        value = value.divide(Conversion.FIVE.getValue(), rounding);
        value = value.add(Conversion.SPLIT.getValue());

        return value.doubleValue();
    }

    public  static double getFahrenheit(double celsius, RoundingMode rounding) {
        BigDecimal value = BigDecimal.valueOf(celsius);

        value = value.subtract(Conversion.SPLIT.getValue());
        value = value.multiply(Conversion.FIVE.getValue());
        value = value.divide(Conversion.NINE.getValue(), rounding);

        return value.doubleValue();

    }


}
