package AreaClassProgram;


public class Area {

    public static double getArea(double radius) {
        return (Math.abs(Math.PI * Math.pow(radius, 2d)));
    }

    public static double getArea(int length, int width) {
        return Math.abs(length * width);
    }

    public static double getArea(double radius, double height) {
        return Math.abs((Math.PI *  Math.pow(radius, 2d)) * height);
    }
}
