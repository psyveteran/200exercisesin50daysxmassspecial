package Circle;

public class Circle {

    private double radius;
    private final double PI = Math.PI;

    public Circle(double radius) {
        this.radius = Math.abs(radius);
    }

    public Circle() {
        radius = 0d;
    }

    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * Math.pow(radius, 2d);
    }

    public double getDiameter() {
        return radius + radius;
    }

    public double getCircumference() {
        return (2 * (PI * radius));
    }
}
