package Circle;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    private Circle circle;

    // Pulled basic test values from Wolfram Alpha.
    private final double[][] values = {
            {11,22,380.133,69.115},
            {87,174,23778.7,546.637},
            {1.0747,2.1494,3.62848,6.75254},
            {33.1, 66.2,3441.96,207.973}};  // radius, diameter, area, perimeter (order).

    @Test
    public void test_getArea() throws Exception {
        assertEquals("[0] Get area test failed.", values[0][2], new Circle(values[0][0]).getArea(), 0.1d);
        assertEquals("[1] Get area test failed.", values[1][2], new Circle(values[1][0]).getArea(), 0.1d);
        assertEquals("[2] Get area test failed.", values[2][2], new Circle(values[2][0]).getArea(), 0.1d);
        assertEquals("[3] Get area test failed.", values[3][2], new Circle(values[3][0]).getArea(), 0.1d);
//        assertEquals(true, false);

    }

    @Test
    public void test_getCircumference() throws Exception {
        assertEquals("[0] Get Circumference diameter test failed.", values[0][3], new Circle(values[0][0]).getCircumference(), 0.01d);
        assertEquals("[0] Get Circumference diameter test failed.", values[1][3], new Circle(values[1][0]).getCircumference(), 0.01d);
        assertEquals("[0] Get Circumference diameter test failed.", values[2][3], new Circle(values[2][0]).getCircumference(), 0.01d);
        assertEquals("[0] Get Circumference diameter test failed.", values[3][3], new Circle(values[3][0]).getCircumference(), 0.01d);
    }

    @Test
    public void test_getDiameter() throws Exception {
        assertEquals("[0] Get diameter test failed.", values[0][1], new Circle(values[0][0]).getDiameter(), 0.0d);
        assertEquals("[1] Get diameter test failed.", values[1][1], new Circle(values[1][0]).getDiameter(), 0.0d);
        assertEquals("[2] Get diameter test failed.", values[2][1], new Circle(values[2][0]).getDiameter(), 0.0d);
        assertEquals("[3] Get diameter test failed.", values[3][1], new Circle(values[3][0]).getDiameter(), 0.0d);
    }

}