package Carpets;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class RoomCarpet {

    class RoomDimension {

        private BigDecimal roomLength;
        private BigDecimal roomWidth;

        public RoomDimension(BigDecimal roomWidth , BigDecimal roomLength) {
            this.roomWidth = roomWidth;
            this.roomLength = roomLength;
        }

        public void setRoomLength(double roomLength) {
            this.roomLength = BigDecimal.valueOf(roomLength).abs();
        }

        public void setRoomWidth(double roomWidth) {
            this.roomWidth = BigDecimal.valueOf(roomWidth).abs();
        }

        public double getArea() {
            return roomLength.multiply(roomWidth).doubleValue();
        }
    }

    private RoomDimension roomDimension;
    private Scanner scanner;

    private final double COST_PER_SQUARE_METER = 9D;

    public RoomCarpet() {
        roomDimension = new RoomDimension(BigDecimal.ZERO, BigDecimal.ZERO);
        scanner = new Scanner(System.in);

        establishRoomSize();
        quote();
    }

    private void quote() {
        System.out.printf("A room with the area of %.3f will cost $%.2f",
                roomDimension.getArea(), getCostQuote());
    }

    private void establishRoomSize() {
        double width = 0d;
        double length = 0d;

        do {
            try {
                System.out.println("Please enter the width of the room for carpeting, followed by the enter key");
                width = scanner.nextDouble();
                width = (width <= 0d) ? 1d : width;
                scanner.nextLine();

                System.out.println("Please enter the length of the room for carpeting, followed by the enter key");
                length = scanner.nextDouble();
                length = (length <= 0) ? 1 : length;
                scanner.nextLine();
            } catch (InputMismatchException ime) {
                System.out.println("You entered invalid information, try again.");
                scanner.nextLine();
            }
        } while (width == 0d | length == 0d);

        roomDimension.setRoomWidth(width);
        roomDimension.setRoomLength(length);
    }

    private double getCostQuote() {
        return COST_PER_SQUARE_METER * roomDimension.getArea();
    }

}
